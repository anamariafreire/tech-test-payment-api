using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly PaymentContext _context;

        public VendedorController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("RegistrarVendedor")]
        public IActionResult Criar(string nome, string cpf, string telefone, string email, Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();

            return Ok(vendedor);
        }

        [HttpGet("ListarVendedores")]
        public IActionResult ListarVendedores()
        {
            var vendedor = _context.Vendedores.ToList();
            if(vendedor == null)
                return NotFound();
            return Ok(vendedor);
        }

        [HttpPut("AtualizarVendedor")]
        public IActionResult Atualizar(int id, Vendedor atualizar)
        {
            var vendedor = _context.Vendedores.Find(id);
            if(vendedor == null)
                return NotFound();

            vendedor.Nome = atualizar.Nome;
            vendedor.Cpf = atualizar.Cpf;
            vendedor.Telefone = atualizar.Telefone;
            vendedor.Email = atualizar.Email;
            

            _context.Vendedores.Update(vendedor);
            _context.SaveChanges();

            return Ok(vendedor);
        }

        [HttpDelete("ExcluirVendedor")]
        public IActionResult Excluir (int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if (vendedor == null)
                return NotFound();
            _context.Vendedores.Remove(vendedor);
            _context.SaveChanges();

            return NoContent();
        }
    }
}