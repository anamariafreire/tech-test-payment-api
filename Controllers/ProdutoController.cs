using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly PaymentContext _context;

        public ProdutoController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("CadastrarProduto")]
        public IActionResult Cadastrar(string nomeproduto)
        {
            Produto produto = new Produto();
            produto.NomeProduto = nomeproduto;
            _context.Add(produto);
            _context.SaveChanges();

            return Ok(produto);
        }
        
        [HttpGet("ListarProdutos")]
        public IActionResult Listar()
        {
            var produto = _context.Produtos.ToList();
            if(produto == null)
                return NotFound();
            return Ok(produto);
        }

        [HttpPut("AtualizarProduto")]
        public IActionResult Atualizar(int id, Produto atualizar)
        {
            var produto = _context.Produtos.Find(id);
            if(produto == null)
                return NotFound();
            produto.NomeProduto = atualizar.NomeProduto;

            _context.Produtos.Update(produto);
            _context.SaveChanges();

            return Ok(produto);
        }
        
        [HttpDelete("ExcluirProduto")]
        public IActionResult Excluir(int id)
        {
            var produto = _context.Produtos.Find(id);
            if(produto == null)
                return NotFound();
            _context.Produtos.Remove(produto);
            _context.SaveChanges();
            return NoContent();
        }
    }
}