using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class VendaController : ControllerBase
    {
        public readonly PaymentContext _context;

        public VendaController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult AdicionarVenda(int IdVendedor, List<Produto> produtos, string nomeproduto)
        {
            Venda venda = new Venda();
            
            venda.ProdutoVenda = new List<Produto>();
            if(_context.Vendedores.Find(IdVendedor) == null)
                return NotFound();
            venda.VendedorId = IdVendedor;
            venda.Data = DateTime.Now;


            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        [HttpGet]
        public IActionResult ObterTodasAsVendas()
        {
            var vendas = _context.Vendas.ToList();
            return Ok(vendas);
        }
        
        [HttpGet("ObterPorId")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
                return NotFound();
            return Ok(venda);
        }

        [HttpPut("AtualizarVenda")]
        public IActionResult AtualizarVenda(int id, EnumVendaStatus status)
        {
            var venda = _context.Vendas.Find(id);
            if(venda == null)
                return NotFound();
            venda.ProdutoVenda = _context.Produtos.Where(x => x.Id == id).ToList();
            if(venda.Status == EnumVendaStatus.AguardandoPagamento && (status == EnumVendaStatus.PagamentoAprovado || status == EnumVendaStatus.Cancelada))
            {
                venda.Status = status;
            }
            else if(venda.Status == EnumVendaStatus.PagamentoAprovado && (status == EnumVendaStatus.EnviadoParaTransportadora || status == EnumVendaStatus.Cancelada))
            {
                venda.Status = status;
            }

            else if(venda.Status == EnumVendaStatus.EnviadoParaTransportadora && status == EnumVendaStatus.Entregue)
            {
                venda.Status = status;
            }
            else
            {
                throw new();
            }

            _context.Vendas.Update(venda);
            _context.SaveChanges();

            return Ok(venda);
        }
    }
}
